import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { finalize, takeUntil } from 'rxjs/operators';
import { LoaderComponent } from '../_components/loader/loader.component';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private dialogService: NbDialogService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
    if(req.reportProgress) {
      return next.handle(req);
    }
    let ref = this.dialogService.open(LoaderComponent);
    return next.handle(req).pipe(finalize(() => {
      setTimeout(() => {
        ref.close();
      }, 500);
    }), takeUntil(ref.onClose));

  }

}