import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHeaders, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { SessionService } from '../_services/session.service';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private session: SessionService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.session.token$.value != null){
      let clone = req.clone({ setHeaders: {"Authorization": 'Bearer ' + this.session.token$.value} })
      return next.handle(clone);
    }
    return next.handle(req);
  }
}
