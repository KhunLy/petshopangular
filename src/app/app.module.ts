import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbDialogModule, NbToastrModule, NbDatepickerModule, NbButtonModule, NbCardModule, NbInputModule, NbIconModule, NbListModule, NbSelectModule, NbActionsModule, NbSpinnerModule, NbAccordionModule, NbCheckboxModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AppComponent } from './app.component';
import { HomeComponent } from './_components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './_components/auth/auth.component';
import { LoginComponent } from './_components/auth/login/login.component';
import { RegisterComponent } from './_components/auth/register/register.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { WorkInProgressComponent } from './_components/work-in-progress/work-in-progress.component';
import { ChangeEmailComponent } from './_components/profile/change-email/change-email.component';
import { ChangePasswordComponent } from './_components/profile/change-password/change-password.component';
import { AnimalComponent } from './_components/animal/animal.component';
import { BreedComponent } from './_components/breed/breed.component';
import { PetComponent } from './_components/pet/pet.component';
import { PetAddComponent } from './_components/pet-add/pet-add.component';
import { BreedAddComponent } from './_components/breed/breed-add/breed-add.component';
import { AnimalAddComponent } from './_components/animal/animal-add/animal-add.component';
import { PetDetailsComponent } from './_components/pet-details/pet-details.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './_interceptors/token.interceptor';
import { LoaderComponent } from './_components/loader/loader.component';
import { LoaderInterceptor } from './_interceptors/loader.interceptor';
import { ConfirmDialogComponent } from './_components/confirm-dialog/confirm-dialog.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ManagePetComponent } from './_components/manage-pet/manage-pet.component';
import { PetDetailsResolver } from './_resolvers/pet-details.resolver';
import { PetUpdateComponent } from './_components/pet-update/pet-update.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    WorkInProgressComponent,
    ChangeEmailComponent,
    ChangePasswordComponent,
    AnimalComponent,
    BreedComponent,
    PetComponent,
    PetAddComponent,
    BreedAddComponent,
    AnimalAddComponent,
    PetDetailsComponent,
    LoaderComponent,
    ConfirmDialogComponent,
    ManagePetComponent,
    PetUpdateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbToastrModule.forRoot(),
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbIconModule,
    NbListModule,
    NbSelectModule,
    NbActionsModule,
    NbSpinnerModule,
    NbAccordionModule,
    NbCheckboxModule,
    MatTableModule,
    MatPaginatorModule,
    ImageCropperModule

  ],
  entryComponents: [
    LoaderComponent,
    ConfirmDialogComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    PetDetailsResolver
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
