import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { AuthComponent } from './_components/auth/auth.component';
import { ProfileComponent } from './_components/profile/profile.component';
import { IsNotLoggedGuard } from './_guards/is-not-logged.guard';
import { IsLoggedGuard } from './_guards/is-logged.guard';
import { IsAdminGuard } from './_guards/is-admin.guard';
import { AnimalComponent } from './_components/animal/animal.component';
import { BreedComponent } from './_components/breed/breed.component';
import { PetDetailsComponent } from './_components/pet-details/pet-details.component';
import { PetAddComponent } from './_components/pet-add/pet-add.component';
import { PetComponent } from './_components/pet/pet.component';
import { PetDetailsResolver } from './_resolvers/pet-details.resolver';
import { PetUpdateComponent } from './_components/pet-update/pet-update.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'auth', component: AuthComponent, canActivate: [IsNotLoggedGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [IsLoggedGuard] },
  { path: 'animal', component: AnimalComponent, canActivate: [IsLoggedGuard, IsAdminGuard] },
  { path: 'breed', component: BreedComponent, canActivate: [IsLoggedGuard, IsAdminGuard] },
  { path: 'pet/add', component: PetAddComponent, canActivate: [IsLoggedGuard, IsAdminGuard] },
  { path: 'pet/:animalName', component: PetComponent },
  { path: 'pet/details/:id', component: PetDetailsComponent, resolve: {model:PetDetailsResolver} },
  { path: 'pet/update/:id', component: PetUpdateComponent, resolve: {model:PetDetailsResolver}, canActivate: [IsLoggedGuard, IsAdminGuard]  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [  ]
})
export class AppRoutingModule { }
