import { Component, OnInit } from '@angular/core';
import { TitleService } from './_services/title.service';
import { Observable } from 'rxjs';
import { NbMenuItem, NbSidebarService, NbToastrService } from '@nebular/theme';
import { SessionService } from './_services/session.service';
import { Router } from '@angular/router';
import { AnimalService } from './_services/animal.service';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  items: NbMenuItem[];

  title$: Observable<string>;

  constructor(
    private titleService: TitleService,
    private sidebar: NbSidebarService,
    public session: SessionService,
    private router: Router,
    private toastrService: NbToastrService,
    private animalService: AnimalService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.refreshSession();
    setInterval(() => this.refreshSession(), 3600000);
    let petGroup = { title: 'Pet', icon: 'shopping-bag-outline', children: [] };
    this.animalService.animal$.subscribe(data => {
      petGroup.children = data.map(a => {
        return { title: a.name, link: '/pet/' + a.name }
      });
    });
    this.title$ = this.titleService.title$;
    this.items = [];
    this.session.tokenModel$.subscribe((data) => {
      this.items = [
        { title: 'Home', icon: 'home-outline', link: '/home' },
        petGroup
      ];
      
      if(data != null && data.RoleName == "ADMIN") {
        petGroup.children.push(
          
        );
        this.items = this.items.concat([
          { title: 'Add Pet', icon: 'plus-outline', link: '/pet/add' },
          { title: 'Breed', icon: 'award-outline', link: '/breed' },
          { title: 'Animal', icon: 'github-outline', link: '/animal' },
        ]);
      }
    })
  }

  toggle() {
    this.sidebar.toggle(true);
  }

  logout() {
    this.session.clear();
    this.router.navigateByUrl('/auth').then(() => {
      this.toastrService.primary("Good Bye")
    });
  }

  private refreshSession() {
    console.log("Refreshing...");
    this.animalService.refresh();
    if(this.session.isLogged$.value) {
      this.authService.refreshToken();
    }
  }

}
