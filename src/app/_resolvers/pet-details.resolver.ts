import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PetModel } from '../_models/pet.model';
import { Observable } from 'rxjs';
import { PetService } from '../_services/pet.service';

@Injectable()
export class PetDetailsResolver implements Resolve<PetModel> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): PetModel | Observable<PetModel> | Promise<PetModel> {
    let id = parseInt(route.params.id);
    return this.petService.get(id);
  }

  constructor(
    private petService: PetService
  ) { }
}
