import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { BreedModel } from 'src/app/_models/breed.model';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { TitleService } from 'src/app/_services/title.service';
import { PetService } from 'src/app/_services/pet.service';
import { AnimalService } from 'src/app/_services/animal.service';
import { BreedService } from 'src/app/_services/breed.service';
import { NbToastrService } from '@nebular/theme';
import { SessionService } from 'src/app/_services/session.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PetModel } from 'src/app/_models/pet.model';

@Component({
  selector: 'app-pet-details',
  templateUrl: './pet-details.component.html',
  styleUrls: ['./pet-details.component.scss']
})
export class PetDetailsComponent implements OnInit {

  data: PetModel;
  apiUrl: string;

  constructor(
    private titleService: TitleService,
    private petService: PetService,
    public animalService: AnimalService,
    private breedService: BreedService,
    private toastrService: NbToastrService,
    public session: SessionService,
    private activedRoute: ActivatedRoute,
  ) { 
    this.apiUrl = environment.apiUrl;
    this.data = this.activedRoute.snapshot.data.model;
  }

  ngOnInit() {
    
  }
}
