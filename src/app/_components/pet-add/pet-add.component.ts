import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/_services/title.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PetService } from 'src/app/_services/pet.service';
import { AnimalService } from 'src/app/_services/animal.service';
import { BreedService } from 'src/app/_services/breed.service';
import { NbToastrService } from '@nebular/theme';
import { BehaviorSubject, Observable } from 'rxjs';
import { BreedModel } from 'src/app/_models/breed.model';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.scss']
})
export class PetAddComponent implements OnInit {

  fg: FormGroup

  breeds$: Observable<BreedModel[]>;

  imageChangedEvent: ImageCroppedEvent;

  croppedImage: any;

  constructor(
    private titleService: TitleService,
    private petService: PetService,
    public animalService: AnimalService,
    private breedService: BreedService,
    private toastrService: NbToastrService,
  ) { 
  }

  ngOnInit() {
    this.breeds$ = new BehaviorSubject([]);
    this.titleService.title = 'Pet - Add';
    this.fg = new FormGroup({
      'reference' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'birthDate' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'breedId' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'image' : new FormControl(null, Validators.compose([
      ])),
      'imageMimeType' : new FormControl(null, Validators.compose([
      ])),
      'vaccinated' : new FormControl(false, Validators.compose([
        Validators.required
      ])),
      'description' : new FormControl(null, Validators.compose([
      ])),
    });
  }

  submit() {
    this.petService.insert(this.fg.value).subscribe(data => {
      this.toastrService.success('New Pet added');
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error));
    });
  }

  changeAnimal(animalName: string) {
    this.breeds$ = this.breedService.getAllByAnimal(animalName);
    this.fg.controls['breedId'].reset();
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      this.fg.controls['image'].setValue(this.croppedImage.replace('data:image/png;base64,', ''));
      this.fg.controls['imageMimeType'].setValue('image/png');
  }

}
