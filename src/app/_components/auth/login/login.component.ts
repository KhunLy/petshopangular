import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { SessionService } from 'src/app/_services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  formGroup: FormGroup;

  constructor(
    private auth: AuthService,
    private router: Router,
    private toastrService: NbToastrService,
    private session: SessionService,
  ) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      'email' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
    });
  }

  submit() {
    this.auth.login(this.formGroup.value).subscribe(data => {
      this.session.start(data);
      this.router.navigateByUrl('/').then(() => this.toastrService.success("Welcome " + this.session.tokenModel$.value.FirstName));
    }, (xhr) => {
      this.toastrService.danger(xhr.error);
    }, () => {

    });
  }

}
