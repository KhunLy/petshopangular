import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { BreedModel } from 'src/app/_models/breed.model';
import { TitleService } from 'src/app/_services/title.service';
import { PetService } from 'src/app/_services/pet.service';
import { AnimalService } from 'src/app/_services/animal.service';
import { BreedService } from 'src/app/_services/breed.service';
import { NbToastrService } from '@nebular/theme';
import { SessionService } from 'src/app/_services/session.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-pet-update',
  templateUrl: './pet-update.component.html',
  styleUrls: ['./pet-update.component.scss']
})
export class PetUpdateComponent implements OnInit {

  fg: FormGroup

  breeds$: Observable<BreedModel[]>;

  imageChangedEvent: ImageCroppedEvent;

  isAdmin: boolean;

  croppedImage: any;

  constructor(
    private titleService: TitleService,
    private petService: PetService,
    public animalService: AnimalService,
    private breedService: BreedService,
    private toastrService: NbToastrService,
    public session: SessionService,
    private activedRoute: ActivatedRoute,
  ) { 
      let data = this.activedRoute.snapshot.data.model;
      console.log(data);
      this.fg = new FormGroup({
        'id' : new FormControl(data.id, Validators.compose([
        ])),
        'reference' : new FormControl(data.reference, Validators.compose([
          Validators.required
        ])),
        'birthDate' : new FormControl(data.birthDate, Validators.compose([
          Validators.required
        ])),
        'breedId' : new FormControl(data.breedId, Validators.compose([
          Validators.required
        ])),
        'image' : new FormControl(data.image, Validators.compose([
        ])),
        'imageMimeType' : new FormControl(data.imageMimeType, Validators.compose([
        ])),
        'vaccinated' : new FormControl(data.vaccinated, Validators.compose([
          Validators.required
        ])),
        'description' : new FormControl(data.description, Validators.compose([
        ])),
      });
      this.breeds$ = new BehaviorSubject([]);
      this.titleService.title = 'Pet - Details';
      this.croppedImage = environment.apiUrl + data.imageUrl;
      this.breeds$ = this.breedService.getAllByAnimal(data.animalName);
      this.breeds$.subscribe(breeds => {
        this.fg.controls['breedId'].setValue(data.breedId);
      });
  }

  ngOnInit() {
    
  }

  submit() {
    this.petService.update(this.fg.value).subscribe(data => {
      console.log(data);
      this.toastrService.success('Pet Updated');
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error));
    });
  }

  changeAnimal(animalName: string) {
    this.breeds$ = this.breedService.getAllByAnimal(animalName);
    this.fg.controls['breedId'].reset();
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
      this.fg.controls['image'].setValue(this.croppedImage.replace('data:image/png;base64,', ''));
      this.fg.controls['imageMimeType'].setValue('image/png');
  }

}
