import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  
  private _item : string;
  public get item() : string {
    return this._item;
  }
  public set item(v : string) {
    this._item = v;
  }
  
  constructor(
    private ref: NbDialogRef<ConfirmDialogComponent>
    ) { }

  ngOnInit() {
  }

  yes() {
    this.ref.close(true);
  }

  no() {
    this.ref.close(false);
  }

}
