import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/_services/animal.service';
import { TitleService } from 'src/app/_services/title.service';
import { AnimalModel } from 'src/app/_models/animal.model';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.scss']
})
export class AnimalComponent implements OnInit {

  displayedColumns: string[] = ['name', 'delete'];

  dataSource: any;

  constructor(
    public animalService: AnimalService,
    private titleService: TitleService,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,
  ) { }

  ngOnInit() {
    this.titleService.title = 'Animal';
    this.animalService.animal$.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    })
  }

  delete(name: string) {
    this.dialogService.open(ConfirmDialogComponent, { context: { 
      item : name
    } }).onClose.subscribe(data => {
      if(data) {
        this.animalService.delete(name).subscribe(data => {
          this.toastrService.success('Delete Completed');
        }, xhr => {
          this.toastrService.danger(JSON.stringify(xhr.error));
        });
      }
    });
  }

  applyFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
  }
}
