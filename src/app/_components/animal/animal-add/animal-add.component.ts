import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AnimalService } from 'src/app/_services/animal.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-animal-add',
  templateUrl: './animal-add.component.html',
  styleUrls: ['./animal-add.component.scss']
})
export class AnimalAddComponent implements OnInit {

  fg: FormGroup;

  constructor(
    private animalService: AnimalService,
    private toastrService: NbToastrService
  ) { }

  ngOnInit() {
    this.fg = new FormGroup({
      'name': new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  submit() {
    this.animalService.insert(this.fg.value).subscribe(() => {
      this.toastrService.success('New Animal Added');
      this.fg.reset();
    }, (xhr) => {
      this.toastrService.danger(JSON.stringify(xhr.error.errors));
    });
  }
}
