import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/_services/title.service';
import { SessionService } from 'src/app/_services/session.service';
import { TokenModel } from 'src/app/_models/token.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/auth.service';
import { NbToastrService } from '@nebular/theme';
import { PetModel } from 'src/app/_models/pet.model';
import { PetService } from 'src/app/_services/pet.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  fg: FormGroup

  connectedUser: TokenModel;

  pets: PetModel[];

  apiUrl: string;

  constructor(
    private titleService: TitleService,
    private session: SessionService,
    private authService: AuthService,
    private toastrService: NbToastrService,
    private petService: PetService,
  ) { }

  ngOnInit() {
    this.apiUrl = environment.apiUrl;
    this.connectedUser = this.session.tokenModel$.value;
    this.titleService.title = this.connectedUser.LastName + ' ' + this.connectedUser.FirstName;
    this.petService.getPetsByUser(parseInt(this.connectedUser.Id)).subscribe(data => {
      this.pets = data;
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error.errors));
    });
    this.fg = new FormGroup({
      id: new FormControl(parseInt(this.connectedUser.Id)),
      lastName: new FormControl(this.connectedUser.LastName, Validators.compose([
        Validators.required
      ])),
      firstName: new FormControl(this.connectedUser.FirstName, Validators.compose([
        Validators.required
      ])),
      birthDate: new FormControl(this.connectedUser.BirthDate, Validators.compose([
      ])),
    })
  }

  submit() {
    this.authService.update(this.fg.value).subscribe(data=> {
      this.toastrService.success("Update Completed");
      this.authService.refreshToken().subscribe(tok => this.session.start(tok));
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error.errors));
    });
  }

  cancel(model: PetModel) {
    this.petService.cancel(model.id).subscribe(data => {
      this.toastrService.primary("Cancellation succedd");
      this.petService.getPetsByUser(parseInt(this.connectedUser.Id)).subscribe(data => {
        this.pets = data;
      }, xhr => {
        this.toastrService.danger(JSON.stringify(xhr.error.errors));
      });
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error));
    });
  }

}
