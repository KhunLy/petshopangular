import { Component, OnInit } from '@angular/core';
import { PetService } from 'src/app/_services/pet.service';
import { PetModel } from 'src/app/_models/pet.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from 'src/app/_services/title.service';
import { environment } from 'src/environments/environment';
import { NbToastrService } from '@nebular/theme';
import { SessionService } from 'src/app/_services/session.service';

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.scss']
})
export class PetComponent implements OnInit {

  model: PetModel[];

  animalName: string;

  apiUrl: string;

  isAdmin: boolean;

  constructor(
    private petService: PetService,
    private activedRoute: ActivatedRoute,
    private titleService: TitleService,
    private router: Router,
    private toastrService: NbToastrService,
    public session: SessionService,
  ) { }

  ngOnInit() {
    this.isAdmin = this.session.tokenModel$.value ? this.session.tokenModel$.value.RoleName == 'ADMIN' : false;
    this.apiUrl = environment.apiUrl;
    this.activedRoute.params.subscribe(p => {
      this.animalName = p.animalName;
      this.titleService.title = 'Pet - ' + this.animalName;
      this.petService.getAllByAnimal(this.animalName).subscribe(
        pets => { this.model = pets; console.log(this.model); }
      );
    });
    
  }

  book(pet: PetModel) {
    this.petService.book(pet.id).subscribe(data => {
      this.router.navigateByUrl('/profile').then(() => {
        this.toastrService.success('Your booking is confirmed');
      });
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error));
    });
  }

}
