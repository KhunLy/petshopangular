import { Component, OnInit } from '@angular/core';
import { BreedModel } from 'src/app/_models/breed.model';
import { BreedService } from 'src/app/_services/breed.service';
import { TitleService } from 'src/app/_services/title.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-breed',
  templateUrl: './breed.component.html',
  styleUrls: ['./breed.component.scss']
})
export class BreedComponent implements OnInit {

  categories: {[key: string]: BreedModel[]};

  displayedColumns: string[] = ['name', 'delete'];

  constructor(
    private breedService: BreedService,
    private titleService: TitleService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
  ) { 
  }

  ngOnInit() {
    this.titleService.title = 'Breed';
    this.refresh();
  }

  delete(id: number) {
    this.dialogService.open(ConfirmDialogComponent, { context: { 
      item : name
    } }).onClose.subscribe(data => {
      if(data) {
        this.breedService.delete(id).subscribe(data => {
          this.refresh();
          this.toastrService.success('Delete Completed');
        }, xhr => {
          this.toastrService.danger(JSON.stringify(xhr.error));
        });
      }
    });
  }

  refresh() {
    this.categories = {};
    this.breedService.getAll().subscribe(items => {
      items.forEach(element => {
        if(this.categories[element.animalName] == null){
          this.categories[element.animalName] = [];
        }
        this.categories[element.animalName].push(element);
      });
    });
  }
}
