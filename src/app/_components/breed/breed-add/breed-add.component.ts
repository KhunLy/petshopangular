import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BreedService } from 'src/app/_services/breed.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AnimalService } from 'src/app/_services/animal.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-breed-add',
  templateUrl: './breed-add.component.html',
  styleUrls: ['./breed-add.component.scss']
})
export class BreedAddComponent implements OnInit {

  @Output() contextChanged: EventEmitter<boolean>;

  fg: FormGroup;

  constructor(
    private breedService: BreedService,
    public animalService: AnimalService,
    private toastrService: NbToastrService,
  ) { 
    this.contextChanged = new EventEmitter<boolean>();
  }

  ngOnInit() {
    this.fg = new FormGroup({
      'name': new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'animalName': new FormControl(null, Validators.compose([
        Validators.required
      ])),
    });
  }

  submit() {
    this.breedService.insert(this.fg.value).subscribe(data => {
      this.toastrService.success('New Breed added');
      this.fg.reset();
      this.contextChanged.emit(true);
    }, xhr => {
      this.toastrService.danger(JSON.stringify(xhr.error));
    });
  }

}
