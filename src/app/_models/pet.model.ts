export interface PetModel {
    id?: number;
    reference: string;
    birthDate: Date;
    breedId: number;
    image: string;
    imageMimeType: string;
    imageUrl: string;
    vaccinated: boolean
    description?: string
    petStatusName: string;
    userId?: number;
    breedName: string;
    animalName: string;
}