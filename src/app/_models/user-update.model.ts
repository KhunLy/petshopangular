export interface UserUpdateModel {
    id: number;
    lastName: string;
    firstName: string;
    birthDate?: Date;
}