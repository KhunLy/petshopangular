export interface UserModel {
    id?: number;
    email: string;
    lastName: string;
    firstName: string;
    birthDate?: Date;
    roleName: string;
}