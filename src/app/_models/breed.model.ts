export interface BreedModel {
    id?: number,
    name: string,
    animalName: string;
}