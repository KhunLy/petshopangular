export interface TokenModel {
  Id: string;
  Email: string;
  LastName: string;
  FirstName: string;
  BirthDate: Date;
  RoleName: string;
  nbf: number;
  exp: number;
  iss: string;
  aud: string;
}