import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { LoginModel } from '../_models/login.model';
import { HttpClient } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { map, finalize, catchError } from 'rxjs/operators';
import { RegisterModel } from '../_models/register.model';
import { environment } from 'src/environments/environment';
import { UserModel } from '../_models/user.model';
import { UserUpdateModel } from '../_models/user-update.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient
  ) { }

  register(user: RegisterModel): Observable<UserModel> {
    return this.httpClient.post<UserModel>(environment.apiUrl + '/user', user);
  }

  update(model: UserUpdateModel): Observable<any> {
    return this.httpClient.put(environment.apiUrl + '/user', model);
  }

  login(login: LoginModel): Observable<string> {
    return this.httpClient.post<string>(environment.apiUrl + '/security/login', login);
  }

  refreshToken(): Observable<string> {
    return this.httpClient.get<string>(environment.apiUrl + '/security/refreshToken', { reportProgress: true });
  }
}
