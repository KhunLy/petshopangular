import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TokenModel } from '../_models/token.model';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  
  private _isLogged$ : BehaviorSubject<boolean>;
  
  public get isLogged$() : BehaviorSubject<boolean> {
    return this._isLogged$;
  }  
  
  private _token$ : BehaviorSubject<string>;

  public get token$() : BehaviorSubject<string> {
    return this._token$;
  }

  private _tokenModel$ : BehaviorSubject<TokenModel>;

  public get tokenModel$() : BehaviorSubject<TokenModel> {
    return this._tokenModel$;
  }

  constructor() {
    let connectedUser = <TokenModel>JSON.parse(localStorage.getItem('TOKEN_MODEL'));
    let date = new Date();
    let timestamp = date.getTime();
    this._isLogged$ = new BehaviorSubject<boolean>(
      connectedUser != null && connectedUser.exp - timestamp/1000 > 0
    );
    this._token$ = new BehaviorSubject<string>(localStorage.getItem('TOKEN'));
    this._tokenModel$ = new BehaviorSubject<TokenModel>(connectedUser);
  }

  start(token: string) {
    localStorage.setItem('TOKEN', token);
    this._token$.next(token);

    let tokenObj =  jwt_decode(token);
    localStorage.setItem('TOKEN_MODEL', JSON.stringify(tokenObj));
    this._tokenModel$.next(tokenObj);

    this._isLogged$.next(true);
  }

  clear() {
    localStorage.clear();
    this._token$.next(null);
    this._tokenModel$.next(null);
    this._isLogged$.next(false);
  }
}
