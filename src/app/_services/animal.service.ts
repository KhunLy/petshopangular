import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AnimalModel } from '../_models/animal.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  private _animal$: BehaviorSubject<AnimalModel[]>;
  public get animal$(): BehaviorSubject<AnimalModel[]> {
    return this._animal$;
  }

  constructor(
    private httpClient: HttpClient
  ) { 
    this._animal$ = new BehaviorSubject<AnimalModel[]>([]);
  }

  refresh() {
    this.httpClient.get<AnimalModel[]>(environment.apiUrl + '/animal', { reportProgress: true })
      .subscribe(data => this._animal$.next(data));
  }

  insert(animalModel: AnimalModel): Observable<any>{
    return this.httpClient.post<AnimalModel[]>(environment.apiUrl + '/animal', animalModel).pipe(finalize(() => {
      this.refresh();
    }));
  }

  delete(animalName: string): Observable<any>{
    return this.httpClient.delete<AnimalModel[]>(environment.apiUrl + '/animal/' + animalName).pipe(finalize(() => {
      this.refresh();
    }));
  }
}
